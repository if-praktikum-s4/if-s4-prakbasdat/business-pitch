# Business Pitch Database

Threads adalah aplikasi media sosial berbasis teks untuk bisa memberikan ruang lebih nyaman kepada para pengguna untuk membuat banyak tulisan berupa threads. Namun jangan khawatir, Threads dari Instagram ini masih bisa mendukung supaya postingan memiliki media foto ataupun video. Di sini saya mencoba memahami fitur dasar dari Threads yang baru saja release ini. Walaupun begitu pengguna Threads sudah langsung mampu mencapai angka 30 juta pengguna di 24 jam pertamanya.

## Threads Design

#### - Prototype Version
![lucid](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/raw/main/img/design/ERD-Threads.png)

#### - Fixed Version
![dbdiagram](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/raw/main/img/design/ERD-Threads2.png)

## 1. Use Case Table

User Use Case
| No. | Use Case                                                    | Status    |
| --- | ----------------------------------------------------------- | --------- |
| 1   | Registrasi                                                  | ✅ Success |
| 2   | Mengubah informasi akun terdaftar                           | ✅ Success |
| 3   | Menghapus akun terdaftar                                    | ✅ Success |
| 4   | Membuat postingan threads                                   | ✅ Success |
| 5   | Mengubah isi konten atau media dari postingan threads       | ✅ Success |
| 6   | Menghapus postingan threads                                 | ✅ Success |
| 7   | Memberikan like ke threads milik pengguna dan pengguna lain | ✅ Success |
| 8   | Menghapus threads like yang telah dibuat                    | ✅ Success |
| 9   | Melakukan replying ke threads pengguna dan pengguna lain    | ✅ Success |
| 10  | Mengubah konten teks reply yang telah dibuat                | ✅ Success |
| 11  | Menghapus reply yang telah dibuat                           | ✅ Success |
| 12  | Melakukan repost/rethreads dari postingan threads           | ✅ Success |
| 13  | Mengubah konten teks dari repost/rethreads                  | ✅ Success |
| 14  | Menghapus repost/rethreads yang telah dibuat                | ✅ Success |
| 15  | Mencari akun pengguna lain dengan username atau fullname    | ✅ Success |

CEO Management Use Case
| No. | Use Case                                                         | Status         |
| --- | ---------------------------------------------------------------- | -------------- |
| 1   | Mendapatkan laporan aktivitas pengguna harian                     | ❗ Planned     |
| 2   | Menganalisis popularitas thread berdasarkan jumlah likes          | ✅ Success    |
| 3   | Menampilkan daftar thread dengan replies terbanyak                | ✅ Success    |
| 4   | Menghitung rata-rata likes per thread                             | ❗ Planned     |
| 5   | Mendapatkan data pengguna berdasarkan usia/tahun kelahiran        | ✅ Success    |
| 6   | Mendapatkan data pengguna dengan jumlah followers terbanyak       | ✅ Success    |
| 7   | Menghitung total threads dan replies per kategori                 | ❗ Planned     |
| 8   | Menampilkan laporan engagement pengguna bulanan                   | ❗ Planned     |
| 9   | Mendapatkan thread berdasarkan tanggal posting                    | ✅ Success    |
| 10  | Mendapatkan pengguna dengan popularitas threads likes terbanyak   | ✅ Success    |
| 11  | Mendapatkan pengguna dengan popularitas threads replies terbanyak | ✅ Success    |
| 12  | Menampilkan laporan popularitas thread berdasarkan kategori       | ❗ Planned     |
| 13  | Mencari thread berdasarkan tag/label                              | ❗ Planned     |
| 14  | Menghitung rata-rata replies per thread dari pengguna             | ❗ Planned     |
| 15  | Menghitung total likes per pengguna dalam rentang waktu           | ✅ Success    |


## 2. CRUD
### - Koneksi ke Database
```javascript
//api-server.js
const express = require('express');
const mysql = require('mysql');
const path = require('path');
const app = express();

const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '********',
  database: 'threadsdb',
});

db.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL database:', err);
    return;
  }
  console.log('Connected to MySQL database');
});
const port = 8080;
app.listen(port, () => {
  console.log(`Server api berjalan pada port ${port}`);
});
```

Hasil testing untuk tersambung ke database sebagai berikut:
```bash
$ node api-server.js
Connected to MySQL database
Server api berjalan pada port 8080
```


### - RESTful API
API ini mengembalikan data pengguna dalam format JSON, yang dapat digunakan oleh aplikasi atau layanan lain atau aplikasi Threads sendiri untuk mengonsumsi dan menampilkan informasi dengan cara yang sesuai dengan melakukan request ke api url.

![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/raw/main/img/api-image/rethreads.png)

Untuk dokumentasi api lainnya: [click here](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/tree/main/img/api-image)
### - Postman Testing
Dokumentasi menunjukkan pengujian berbagai jenis permintaan API dengan menggunakan Postman. Permintaan tersebut mencakup seperti `GET`, `POST`, `DELETE`, `PUT`, `HEAD`, `PATCH`, dan `OPTIONS`, dan ditujukan ke URL yang sesuai dengan fungsi API yang ingin diuji.

![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/raw/main/img/postman-test/postman-users.png)

Untuk dokumentasi Postman lainnya: [click here](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/tree/main/img/postman-test)
## 3. Visualisasi Data: Business Intelligence Tool

Untuk memvisualisasikan data dalam database Threads, saya menggunakan alat Business Intelligence yang disebut Metabase. Dengan Metabase, kita dapat mengakses berbagai jenis visualisasi seperti diagram batang, grafik garis, diagram lingkaran, peta, dan banyak lagi. Anda dapat menjelajahi data Anda, membuat laporan, dan mendapatkan informasi berharga tentang pengguna, threads, likes, replies, dan rethreads di database Threads.

Metabase juga menawarkan fitur-fitur canggih seperti drill-down, filter, dan pembuatan pertanyaan interaktif yang memungkinkan untuk menggali lebih dalam ke dalam data dan menjawab pertanyaan bisnis yang relevan. Berikut beberapa contoh visualisasi data

### - Jumlah likes dari postingan Threads yang memiliki likes
![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/raw/main/img/metabase-visualization/visualisasi-likes.png)
### - Jumlah threads yang diposting oleh pengguna
![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/raw/main/img/metabase-visualization/visualisasi-threads-from-users.png)

Untuk dokumentasi visualisasi data lainnya: [click here](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/tree/main/img/metabase-visualization)
## 4. Built-in Function
### - Regex
Fungsi-fungsi REGEXP memberikan kemampuan yang kuat dalam mencocokkan dan memanipulasi data menggunakan ekspresi reguler. Ini juga bisa digunakan untuk mencari Threads yang sensitive, seperti menggunakan istilah-istilah suku, agama, ras, dan antargolongan (SARA) atau toxic dan kata-kata tidak pantas lainnya.
```sql
SELECT * FROM threads WHERE threads_text REGEXP 'Allah | Yesus | Mati | Islam';
```

### - Substring
Substring digunakan untuk mengambil substring atau sebagian saja dari sebuah string. Seperti pada contoh di bawah ini, yang mengambil 4 digit saja dari kolom `birthday` dengan value 2003.

```sql
SELECT username AS Username, fullname AS `Nama Lengkap`, COUNT(*) AS `Jumlah Threads`, bio AS `Bio Profil`
FROM users
WHERE SUBSTRING(birthday, 1, 4) = '2003'
GROUP BY username, fullname, bio;
```
### Other Built-in Function
Penggunaan `COUNT` untuk querying pengguna dengan likes pada postingan threads terbanyak.
```sql
SELECT
  ROW_NUMBER() OVER (ORDER BY t.likes DESC) AS `No.`,
  u.fullname AS `Nama Lengkap`,
  COUNT(DISTINCT th.id) AS `Jumlah Threads`,
  t.likes AS `Likes`
FROM
  users u
  INNER JOIN (
    SELECT
      th.user_id,
      COUNT(tl.id) AS likes
    FROM
      threads th
      LEFT JOIN threads_likes tl ON th.id = tl.threads_id
    GROUP BY
      th.user_id
  ) t ON u.id = t.user_id
  INNER JOIN threads th ON u.id = th.user_id
GROUP BY
  u.id, u.fullname, t.likes
ORDER BY
  `Likes` DESC;
```
Database akan memberikan informasi berikut:
| No. | Nama Lengkap         | Jumlah Threads | Likes |
| --- | ------------------- | -------------- | ----- |
| 1   | Perempuan Hijrah    | 10             | 314   |
| 2   | Ridwan Ahmad Fauzan | 6              | 112   |
| 3   | Raden Ibnu          | 6              | 0     |
| 4   | Luthfi Idhar        | 6              | 0     |
| 5   | Cindy Rahmatunnisa  | 6              | 0     |
| 6   | Sandya Yudha        | 6              | 0     |
| 7   | Elmi Wahyu          | 6              | 0     |
| 8   | Rifqi Syekhi        | 6              | 0     |
| 9   | Dian Julianti       | 6              | 0     |
| 10  | Sophia Johnson      | 6              | 0     |

## 5. Sub-query 
Salah satu contoh Sub-query yang digunakan dalam **Aplikasi Threads App by Instagram** adalah menampilkan sebuah Threads beserta Replies-replies dari Threads utamanya.
```sql
SELECT
  CONCAT('@', u.username) AS Username,
  t.threads_text AS Threads,
  t.timestamp AS Time
FROM
  threads t
JOIN
  users u ON u.id = t.user_id
WHERE
  t.id = 19
UNION
SELECT
  CONCAT('@', u.username) AS Username,
  tr.reply_text AS Threads,
  tr.timestamp AS Time
FROM
  threads_replies tr
JOIN
  users u ON u.id = tr.user_id
WHERE
  tr.parent_reply_id IN (
    SELECT id
    FROM threads_replies
    WHERE threads_id = 19
      AND user_id = 1
  );
  ```
Query tersebut menggabungkan data dari tabel threads dan tabel threads_replies menggunakan UNION. Pertama, query memilih data threads utama dengan ID tertentu dan memilih kolom username, threads_text, dan timestamp dari tabel threads. Kemudian, query kedua memilih data replies replies dengan parent_reply_id yang sesuai dengan ID dari subquery yang memenuhi kondisi threads_id = 19 dan user_id = 1. Hasil akhir adalah gabungan antara threads utama dan replies replies. Tampilan seperti berikut:
| Username       | Threads                                                                                                                  | Time               |
| -------------- | ------------------------------------------------------------------------------------------------------------------------ | ------------------ |
| @r1dwanafazn   | Apakah ada yang suka berkebun di sini?                                                                                   | 2023-07-15 00:15:02|
| @r1dwanafazn   | jadii semaian yang berhasil itu kalau sinar mataharinya tercukupi                                                        | 2023-07-15 11:05:53|
| @r1dwanafazn   | biasanya kalau udah berkecambah dikit aja, langsung aku kenakan sinar matahari terutama sinar pagi                       | 2023-07-15 11:05:53|
| @r1dwanafazn   | bibit yang bagus: 1. daunnya bagus seger ijo lebar 2. batangnya tidak panjang                                            | 2023-07-15 11:05:53|
| @cndyrtnsaa    | kak mau tanya dong, kalo bercocok tanam di atap lokasi dataran rendah perlu pakai paranet ga? kalo ya yang berapa persen? | 2023-07-15 11:39:17|
| @elmijjj       | kalo semaian ada ulet kaya gini gimana min                                                                               | 2023-07-15 11:50:07|
| @reskifirmansyah | min mo tanyaa.. kan udah semai nih tapi kalo kena matahari menguning, kalo ngga malah layu                               | 2023-07-15 11:50:07|
| @reskifirmansyah | aku nanam pakai tanah campur sekam di polibag, kena sinar mataharinya pagi sampe sore                                    | 2023-07-15 11:50:07|

## 6. Transaction
Berikut percobaan melakukan TRANSACTION pada database, yang akan mengubah username dari akun terdaftar `@sandyayudha12` menjadi `@ohmysandya44`, dan penambahan threads like terhadap threads yang telah di post oleh sandyayudha12 sebelumnya.
```sql
UPDATE users SET username = 'ohmysandya44' WHERE username = 'sandyayudha12';

IF ROW_COUNT() > 0 THEN
    INSERT INTO threads_likes (user_id, threads_id)
    SELECT id, 5
    FROM users
    WHERE username = 'ohmysandya44';

    IF ROW_COUNT() > 0 THEN
        COMMIT;
        SELECT 'All operations successfully committed.';
    ELSE
        ROLLBACK;
        SELECT 'Error occurred. All operations rolled back.';
    END IF;
ELSE
    ROLLBACK;
    SELECT 'No rows updated. Transaction rolled back.';
END IF;
```

## 7. Procedure (Function) & Trigger
### - Procedure untuk menghitung jumlah threads yang dimiliki oleh User
```sql
DELIMITER //
CREATE PROCEDURE CountUserThreads(IN userId INT, OUT threadCount INT)
BEGIN
    SELECT COUNT(*) INTO threadCount
    FROM threads
    WHERE user_id = userId;
END //
DELIMITER ;

```
Cara memanggil
```sql
CALL CountUserThreads(1, @threadCount);
SELECT @threadCount;
```

### - Procedure untuk meng-update bio profil pengguna 
```sql
DELIMITER //
CREATE PROCEDURE UpdateUserBio(IN userId INT, IN newBio TEXT)
BEGIN
    UPDATE users
    SET bio = newBio
    WHERE id = userId;
END //
DELIMITER ;

```
Cara memanggil
```sql
CALL UpdateUserBio(1, 'Bio baru pengguna');
```

### - Trigger 
#### Menghapus data terkait Threads
```sql
DELIMITER //
CREATE TRIGGER delete_related_data
AFTER DELETE ON threads
FOR EACH ROW
BEGIN
    -- Menghapus rethreads yang terkait
    DELETE FROM re_threads WHERE threads_id = OLD.id;
    -- Menghapus replies yang terkait
    DELETE FROM threads_replies WHERE threads_id = OLD.id;
    -- Menghapus likes yang terkait
    DELETE FROM threads_likes WHERE threads_id = OLD.id;
END //
DELIMITER ;
```
#### Menghapus data terkait users
```sql
DELIMITER //
CREATE TRIGGER delete_user_threads
AFTER DELETE ON users
FOR EACH ROW
BEGIN
    -- Menghapus threads yang dibuat oleh user
    DELETE FROM threads WHERE user_id = OLD.id;
END //
DELIMITER ;
```

## 8. Data Control Language
DCL (Data Control Language) adalah bagian dari bahasa SQL (Structured Query Language) yang digunakan untuk mengatur dan mengontrol hak akses pengguna ke objek database, seperti tabel, view, prosedur, dan lainnya. DCL terdiri dari perintah-perintah seperti GRANT, REVOKE, dan beberapa perintah terkait keamanan dan izin.

Dengan DCL, administrator database dapat memberikan izin akses tertentu kepada pengguna, mengatur hak akses berdasarkan peran atau tanggung jawab, menerapkan kebijakan keamanan seperti pembatasan akses ke data sensitif, dan mengontrol operasi yang dapat mempengaruhi integritas data. DCL membantu menjaga keamanan, kerahasiaan, dan integritas data dalam lingkungan database.

### - CREATE
```sql
CREATE USER 'superAdmin'@'localhost' IDENTIFIED BY 'superAdmin1239';=
FLUSH PRIVILEGES;
```
### - GRANT
```sql
GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, CREATE, DROP ON threads_try.* TO 'superAdmin'@'localhost';
```
### - REVOKE
```sql
REVOKE DELETE ON threads_try.* FROM 'superAdmin'@'localhost';
FLUSH PRIVILEGES;
```
### - LOGIN
Setelah sebuah user dibuat dan beberapa privilege ditambah dan dikurangkan, kita bisa login dengan user tersebut dengan format `mysql -u <nama-user> -p` setelah itu masukkan password dari user terkait.
```bash
mysql -u superAdmin -p
```
### - DROP
```sql
DROP USER 'superAdmin'@'localhost';
FLUSH PRIVILEGES;
```
FLUSH PRIVILEGES digunakan untuk memuat ulang file izin (privilege) dan menghentikan kegiatan cache izin yang sedang berjalan. Ketika perubahan izin (privilege) dilakukan seperti GRANT, REVOKE, dan lainnya, FLUSH PRIVILEGES digunakan untuk memastikan bahwa perubahan-perubahan tersebut diterapkan secara langsung oleh server MySQL.

## 9. Constraint
Untuk penggunaan `CONSTRAINT` dalam project ini bisa secara lengkap dilihat pada DDL SQL Code berikut ini: [click here](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/business-pitch/-/blob/main/src/sql/ddl-threadsdb.sql) 

Dengan rincian
1. Primary Key [5]
2. Unique Key [2]
3. Foreign Key [8]

atau lihat di bawah ini:
```sql
CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  email VARCHAR(100) UNIQUE NOT NULL, -- unique key
  username VARCHAR(50) UNIQUE NOT NULL, -- unique key
  fullname VARCHAR(100) NOT NULL,
  birthday DATE,
  profile_picture VARCHAR(200),
  bio TEXT
);
CREATE TABLE threads (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  threads_text TEXT NOT NULL,
  media VARCHAR(200),
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id) -- foreign key
);
CREATE TABLE threads_likes (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  user_id INT NOT NULL,
  threads_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id), -- foreign key
  FOREIGN KEY (threads_id) REFERENCES threads(id) -- foreign key
);
CREATE TABLE re_threads (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  quote_text TEXT,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT NOT NULL,
  threads_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id), -- foreign key
  FOREIGN KEY (threads_id) REFERENCES threads(id) -- foreign key
);
CREATE TABLE threads_replies (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  reply_text TEXT NOT NULL,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT,
  threads_id INT,
  parent_reply_id INT,
  FOREIGN KEY (user_id) REFERENCES users(id), -- foreign key
  FOREIGN KEY (threads_id) REFERENCES threads(id), -- foreign key
  FOREIGN KEY (parent_reply_id) REFERENCES threads_replies(id) -- foreign key
);

```

## 10. YouTube Demonstration
Video via: [YouTube](https://youtube.com/playlist?list=PLwAKAPYbtOjhQospYZR50Ql6-tnWxxi8I)

## 11. UI for CRUD
It's a shame, there's no CRUD via GUI 🥲🙏🏻
