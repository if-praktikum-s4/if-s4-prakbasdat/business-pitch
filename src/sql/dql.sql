-- MAIN QUERY OF ALL
show tables;
select*from users;
select*from threads;
select*from threads_likes;
select*from threads_replies;
select*from re_threads;


-- REGEXP
SELECT * FROM threads WHERE threads_text REGEXP 'Allah | Yesus | Mati | Islam';

-- SUBSTRING
SELECT username AS Username, fullname AS `Nama Lengkap`, COUNT(*) AS `Jumlah Threads`, bio AS `Bio Profil`
FROM users
WHERE SUBSTRING(birthday, 1, 4) = '2003'
GROUP BY username, fullname, bio;

-- OTHER
SELECT
  ROW_NUMBER() OVER (ORDER BY t.likes DESC) AS `No.`,
  u.fullname AS `Nama Lengkap`,
  COUNT(DISTINCT th.id) AS `Jumlah Threads`,
  t.likes AS `Likes`
FROM
  users u
  INNER JOIN (
    SELECT
      th.user_id,
      COUNT(tl.id) AS likes
    FROM
      threads th
      LEFT JOIN threads_likes tl ON th.id = tl.threads_id
    GROUP BY
      th.user_id
  ) t ON u.id = t.user_id
  INNER JOIN threads th ON u.id = th.user_id
GROUP BY
  u.id, u.fullname, t.likes
ORDER BY
  `Likes` DESC;

 -- SUBQUERY
 SELECT
  CONCAT('@', u.username) AS Username,
  t.threads_text AS Threads,
  t.timestamp AS Time
FROM
  threads t
JOIN
  users u ON u.id = t.user_id
WHERE
  t.id = 19
UNION
SELECT
  CONCAT('@', u.username) AS Username,
  tr.reply_text AS Threads,
  tr.timestamp AS Time
FROM
  threads_replies tr
JOIN
  users u ON u.id = tr.user_id
WHERE
  tr.parent_reply_id IN (
    SELECT id
    FROM threads_replies
    WHERE threads_id = 19
      AND user_id = 1
  );
