INSERT INTO threads (threads_text, media, user_id)
VALUES
('Halo semuanya!', NULL, 1),
('Saya baru saja menyelesaikan proyek besar!', 'project.jpg', 2),
('Apakah ada rekomendasi buku bagus?', NULL, 3),
('Ingin berbagi pengalaman traveling ke Bali', 'bali.jpg', 4),
('Mari diskusikan teknologi terkini', NULL, 5),
('Tips untuk menjaga kesehatan tubuh', NULL, 6),
('Saya menonton konser favorit saya malam ini!', 'concert.jpg', 7),
('Inspirasi fashion untuk musim panas', NULL, 8),
('Ceritakan pengalaman mendaki Gunung Rinjani', NULL, 10);

desc threads;
select*from users u ;
SELECT*FROM threads;

INSERT INTO threads (threads_text, user_id)
VALUES
('kenal - taaruf - nikah - kaya - punya anak hafiz - mati - masuk surga, sesederhana itu kok yaa Allah.', 159),
('Cerita dong keajaiban "tahajud" yang pernah kamu alamin', 159),
('Orang kalau udah cinta, melihat sendalnya aja udah seneng..', 159),
('Doa jodohku kuat banget ya, buktinya aku masih betah sendiri sampe sekarang', 159),
('1% kuat, 99% modal ya Allah ya Allah setiap hari.', 159),
('Makin dewasa makin sadar, yang dibutuhin cuman sholat tepat waktu dan duit 10 miliar.', 159),
('Ngejar dhuha sama tahajud aja masih susah, yakali ngerjar kamu..', 159),
('Curhat sama Allah tuh aman, nyaman, tenang, dan gak akan dibanding bandingin.', 159),
('Lu punya duit, lu punya kuasa. Bagilu, gue punya Allah gue punya segalanya.', 159),
('Percaya atau engga, anak perempuan yang hidupnya perih dari kecil, derajatnya akan diangkat oleh Allah melalui pasangannya', 159);


INSERT INTO threads (threads_text, media, user_id)
VALUES
('Siapa yang suka memasak? Bagikan resep favoritmu!', NULL, 1),
('Sedang mencari film rekomendasi nih, ada yang punya?', NULL, 2),
('Diskusi tentang olahraga favor	it kamu', NULL, 3),
('Mengenal budaya Jawa Tengah', 'culture.jpg', 4),
('Bagaimana pendapat kalian tentang teknologi AI?', NULL, 5),
('Tips menjaga kesehatan mental di era digital', NULL, 6),
('Review film terbaru yang baru saja saya tonton', 'movie.jpg', 7),
('OOTD (Outfit of the day) hari ini!', NULL, 8),
('Pengalaman camping seru di pegunungan', NULL, 10),
('Apakah ada yang suka berkebun di sini?', NULL, 1),
('Membahas novel terbaru karya penulis Indonesia', NULL, 2),
('Rekomendasi playlist musik yang enak didengar', NULL, 3),
('Foto-foto indah dari perjalanan ke Pulau Lombok', 'lombok.jpg', 4),
('Apa yang bisa kita harapkan dari teknologi 5G?', NULL, 5),
('Tips menjaga kebugaran di tengah kesibukan', NULL, 6),
('Live report dari konser favorit malam ini!', 'concert.jpg', 7),
('Inspirasi outfit untuk acara formal', NULL, 8),
('Panduan lengkap mendaki Gunung Semeru', NULL, 10),
('Resep kue kering untuk Lebaran', NULL, 1),
('Diskusi tentang film favorit tahun ini', NULL, 2),
('Perkembangan terbaru dalam dunia sepak bola', NULL, 3),
('Keindahan tarian tradisional Sumatera Barat', 'dance.jpg', 4),
('Apa yang membuat Anda tertarik dengan teknologi blockchain?', NULL, 5),
('Cara mengatur waktu dengan efektif', NULL, 6),
('Ulasan album musik baru yang saya dengar', 'album.jpg', 7),
('Outfit casual yang nyaman untuk sehari-hari', NULL, 8),
('Pengalaman mendaki Gunung Bromo', NULL, 10),
('Tips membuat kue basah untuk Lebaran', NULL, 1),
('Rekomendasi film thriller yang menegangkan', NULL, 2),
('Diskusi tentang cabang olahraga favoritmu', NULL, 3),
('Wisata kuliner di kota Yogyakarta', 'culinary.jpg', 4),
('Perkembangan teknologi Internet of Things (IoT)', NULL, 5),
('Strategi menjaga keseimbangan kehidupan dan pekerjaan', NULL, 6),
('Konser musik indie yang seru tadi malam!', 'concert.jpg', 7),
('Inspirasi outfit untuk musim gugur', NULL, 8),
('Petualangan seru di Taman Nasional Komodo', NULL, 10),
('Resep minuman segar untuk cuaca panas', NULL, 1),
('Rekomendasi film romantis yang bisa mengharukan', NULL, 2),
('Diskusi tentang cabang olahraga ekstrem', NULL, 3),
('Pesona seni tari Bali yang memukau', 'dance.jpg', 4),
('Apa yang akan kita lihat di masa depan teknologi VR?', NULL, 5),
('Cara menjaga kesehatan saat bekerja dari rumah', NULL, 6),
('Review konser band favorit yang luar biasa', 'concert.jpg', 7),
('Inspirasi outfit untuk musim dingin', NULL, 8),
('Petualangan di Kawah Ijen yang menakjubkan', NULL, 10);
