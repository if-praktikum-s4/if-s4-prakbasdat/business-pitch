ALTER TABLE threads_replies AUTO_INCREMENT = 1;


use threads_try;
desc threads_replies ;
select*from threads_replies tr ;
select*from threads t 
where id = 3;


INSERT INTO threads_replies (reply_text, user_id, threads_id, parent_reply_id)
VALUES
('aku mau berbagi sedikit cerita nih ke temen-temen tentang pengalamaan aku pas berkebun di rumah', 1, 19, NULL),
('jadii semaian yang berhasil itu kalau sinar mataharinya tercukupi', 1, 19, 1),
('biasanya kalau udah berkecambah dikit aja, langsung aku kenakan sinar matahari terutama sinar pagi', 1, 19, 2),
('"bibit yang bagus: 1. daunnya bagus seger ijo lebar 2. batangnya tidak panjang', 1, 19, 3),
('kak mau tanya dong, kalo bercocok tanam di atap lokasi dataran rendah perlu pakai paranet ga? kalo ya yang berapa persen?', 4, 19, 4),
('kalo menurut pengalaman aku, coba jangan pakai, kalau hasilnya suka kering, baru pakaikan paranet', 5, 19, 1);

INSERT INTO threads_replies (reply_text, parent_reply_id, threads_id, user_id)
VALUES
('kalo semaian ada ulet kaya gini gimana min', 6, 19, 6),
('amal', 7, 19, 1),
('min mo tanyaa.. kan udah semai nih tapi kalo kena matahari menguning, kalo ngga malah layu', 8, 19, 132),
('nanemnya di tanah atau rockwoll kak? sinarnya pagi atau sore? ', 9, 19, 1),
('aku nanam pakai tanah campur sekam di polibag, kena sinar mataharinya pagi sampe sore', 10, 19, 132);

INSERT INTO threads_replies (reply_text, user_id, threads_id, parent_reply_id)
VALUES
('Jangan numpuk banyak hal yang dilakuin, maksimal ada tiga tugas saja', 3, 3, NULL),
('Anggap yang dilakukan ini berharga dan gapapa kalau ngga nyaman, supaya ada pembelajaran', 3, 3, 12),
('Apresiasi peningkatan kecil, biar tahu porsinya bisa sampai dimana', 3, 3, 13),
('Hargai hal-hal sederhana dari langkah dan kenali masalah sederhana, biar berkurang rasa takutnya👻', 3, 3, 14),
('Further reading-nya buku-buku ini yha👇🏻', 3, 3, 15),
('- Buku: Jangan Memulai Apa yang Tidak Bisa Kamu Selesaikan by PETER HOLLINS', 3, 3, 16),
('- One Small Step Can Change Your Life', 3, 3, 17);

INSERT INTO threads_replies (reply_text, user_id, threads_id, parent_reply_id)
VALUES
('makasih banyak tipsnya ngebantu sekali!🙂🤌🏻', 130, 3, 18),
('sama-sama yaa ray', 3, 3, 19),
('waa jadi kepoo sama yang one small step. Udah punya tinggal buka plastik wkwk', 144, 3, 20),
('sama kak aku penasaran, itu beli dimana ya? hihi siiiuuuu 😲😂', 116, 3, 21);
