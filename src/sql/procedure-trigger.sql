-- TRANSACTION
UPDATE users SET username = 'ohmysandya44' WHERE username = 'sandyayudha12';

IF ROW_COUNT() > 0 THEN
    INSERT INTO threads_likes (user_id, threads_id)
    SELECT id, 5
    FROM users
    WHERE username = 'ohmysandya44';

    IF ROW_COUNT() > 0 THEN
        COMMIT;
        SELECT 'All operations successfully committed.';
    ELSE
        ROLLBACK;
        SELECT 'Error occurred. All operations rolled back.';
    END IF;
ELSE
    ROLLBACK;
    SELECT 'No rows updated. Transaction rolled back.';
END IF;

-- PROCEDURE COUNT THREADS OF USER
DELIMITER //
CREATE PROCEDURE CountUserThreads(IN userId INT, OUT threadCount INT)
BEGIN
    SELECT COUNT(*) INTO threadCount
    FROM threads
    WHERE user_id = userId;
END //
DELIMITER ;

-- USE IT?
CALL CountUserThreads(1, @threadCount);
SELECT @threadCount;

-- BIO UPDATER
DELIMITER //
CREATE PROCEDURE UpdateUserBio(IN userId INT, IN newBio TEXT)
BEGIN
    UPDATE users
    SET bio = newBio
    WHERE id = userId;
END //
DELIMITER ;

-- USE IT?
CALL UpdateUserBio(1, 'Bio baru pengguna');

-- TRIGGER DELETE IF DELETING THREADS
DELIMITER //
CREATE TRIGGER delete_related_data
AFTER DELETE ON threads
FOR EACH ROW
BEGIN
    -- Menghapus rethreads yang terkait
    DELETE FROM re_threads WHERE threads_id = OLD.id;
    -- Menghapus replies yang terkait
    DELETE FROM threads_replies WHERE threads_id = OLD.id;
    -- Menghapus likes yang terkait
    DELETE FROM threads_likes WHERE threads_id = OLD.id;
END //
DELIMITER ;

-- DELETE THREADS IF DELETE USER
DELIMITER //
CREATE TRIGGER delete_user_threads
AFTER DELETE ON users
FOR EACH ROW
BEGIN
    -- Menghapus threads yang dibuat oleh user
    DELETE FROM threads WHERE user_id = OLD.id;
END //
DELIMITER ;
