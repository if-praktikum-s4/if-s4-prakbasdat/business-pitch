INSERT INTO re_threads (quote_text, user_id, threads_id)
VALUES
('Great post!', 2, 1),
('Congratulations!', 3, 3);

SELECT*FROM re_threads;
desc re_threads ;
select*from threads t ;

-- Lu punya duit, lu punya kuasa. Bagiku, gue punya Allah gue punya segalanya.
INSERT INTO re_threads (quote_text, user_id, threads_id)
VALUES
('Bijak sekali, memang kekayaan adalah kuasa. Bagilah ya!', 1, 63),
('Setuju banget! Kepercayaan pada Allah adalah segalanya.', 2, 63),
('Sangat bijak! Allah adalah segalanya dalam hidup kita.', 3, 63),
('Kuatnya motivasi dari kalimat ini. Bagilah apa yang kamu miliki!', 4, 63),
('Hidup memang lebih bermakna dengan keyakinan pada Allah.', 5, 63),
('Betul sekali, Allah-lah yang memiliki segalanya.', 6, 63),
('Sangat inspiratif! Allah adalah sumber kekuatan sejati.', 7, 63),
('Bagi duitmu, bagilah yang kamu miliki. Allah akan memberikan lebih.', 8, 63),
('Maknanya sangat dalam, Allah adalah segalanya.', 116, 63),
('Luar biasa! Bagilah apa yang kamu punya, Allah akan memberikan lebih.', 117, 63),
('Bijak sekali! Keimanan pada Allah adalah segalanya.', 118, 63),
('Betul banget, segalanya milik Allah.', 119, 63),
('Inspiratif sekali! Bagilah apa yang kamu miliki.', 120, 63),
('Allah adalah sumber kekuatan dan keberkahan sejati.', 121, 63),
('Terinspirasi sekali dengan kalimat ini. Bagilah rezekimu.', 122, 63),
('Sangat bijak! Allah-lah yang memiliki segalanya.', 123, 63),
('Motivasi yang kuat. Bagilah apa yang kamu punya.', 124, 63),
('Sangat menginspirasi! Allah adalah segalanya dalam hidup kita.', 125, 63),
('Bagilah apa yang kamu miliki, Allah akan memberikan lebih.', 126, 63),
('Luar biasa! Keyakinan pada Allah adalah segalanya.', 127, 63),
('Sangat inspiratif! Allah adalah sumber kekuatan sejati.', 128, 63),
('Betul sekali! Bagilah duitmu, Allah akan memberikan lebih.', 129, 63),
('Inspiratif sekali! Allah adalah segalanya.', 130, 63),
('Bijak banget! Kepercayaan pada Allah adalah kunci.', 131, 63),
('Sangat inspiratif! Allah adalah sumber kekuatan dalam hidup kita.', 132, 63),
('Motivasi yang kuat! Bagilah apa yang kamu punya.', 133, 63),
('Terinspirasi sekali dengan kalimat ini. Allah-lah yang memiliki segalanya.', 134, 63),
('Bagilah duitmu, Allah akan memberikan lebih padamu.', 135, 63),
('Sangat bijak! Allah adalah segalanya dalam hidup kita.', 136, 63),
('Inspiratif sekali! Bagilah apa yang kamu miliki.', 137, 63),
('Bijak sekali! Allah adalah sumber kekuatan dalam hidup kita.', 138, 63),
('Luar biasa! Bagilah apa yang kamu punya, Allah akan memberikan lebih.', 139, 63),
('Motivasi yang kuat! Keimanan pada Allah adalah segalanya.', 140, 63),
('Sangat menginspirasi! Allah adalah sumber kekuatan sejati.', 141, 63),
('Bagilah rezekimu, Allah akan memberikan lebih.', 142, 63),
('Inspiratif sekali! Allah-lah yang memiliki segalanya.', 143, 63),
('Bijak banget! Bagilah apa yang kamu miliki.', 144, 63);


-- Makin dewasa makin sadar, yang dibutuhin cuman sholat tepat waktu dan duit 10 miliar.
INSERT INTO re_threads (quote_text, user_id, threads_id)
VALUES
('Hahaha, betul banget! Sholat tepat waktu jadi prioritas utama.', 1, 60),
('Perjuangan sejati! Sholat tepat waktu dan duit 10 miliar.', 2, 60),
('Mantap! Sholat tepat waktu dan kekayaan, perjuangan hidup yang sebenarnya.', 3, 60),
('Hahaha, kalimat yang menghibur! Sholat tepat waktu dan duit 10 miliar ya.', 4, 60),
('Benar sekali! Sholat tepat waktu dan rejeki melimpah.', 8, 60),
('Hahaha, kocak! Sholat tepat waktu dan impian 10 miliar.', 116, 60),
('Persis banget! Sholat tepat waktu dan keberuntungan 10 miliar.', 117, 60),
('Hahaha, betul sekali! Sholat tepat waktu dan duit 10 miliar, perjuangan hidup kita.', 118, 60),
('Lucu banget! Sholat tepat waktu dan impian menjadi kenyataan.', 119, 60),
('Tepat sekali! Sholat tepat waktu dan kekayaan yang melimpah.', 120, 60),
('Hahaha, kocak abis! Sholat tepat waktu dan impian besar.', 121, 60),
('Percaya banget! Sholat tepat waktu dan rejeki berlimpah.', 122, 60),
('Hahaha, kalimat yang menghibur! Sholat tepat waktu dan duit 10 miliar, cita-cita hidup.', 123, 60),
('Persis banget! Sholat tepat waktu dan kesuksesan besar.', 124, 60),
('Hahaha, betul sekali! Sholat tepat waktu dan duit 10 miliar, perjalanan hidup yang sebenarnya.', 125, 60),
('Kocak abis! Sholat tepat waktu dan impian 10 miliar.', 126, 60),
('Hahaha, lucu banget! Sholat tepat waktu dan kekayaan melimpah.', 127, 60),
('Percaya banget! Sholat tepat waktu dan impian besar.', 128, 60),
('Hahaha, betul sekali! Sholat tepat waktu dan duit 10 miliar, perjuangan nyata.', 129, 60),
('Persis banget! Sholat tepat waktu dan keberuntungan besar.', 130, 60),
('Hahaha, kalimat yang menghibur! Sholat tepat waktu dan impian 10 miliar, kehidupan sejati.', 131, 60),
('Percaya banget! Sholat tepat waktu dan rejeki melimpah.', 132, 60),
('Hahaha, kocak abis! Sholat tepat waktu dan duit 10 miliar, perjalanan hidup.', 133, 60),
('Benar sekali! Sholat tepat waktu dan kesuksesan besar.', 134, 60),
('Hahaha, betul sekali! Sholat tepat waktu dan duit 10 miliar, perjuangan sejati.', 135, 60),
('Kocak abis! Sholat tepat waktu dan impian 10 miliar, kehidupan penuh arti.', 136, 60);

-- Apakah ada rekomendasi buku bagus?
INSERT INTO re_threads (quote_text, user_id, threads_id)
VALUES
('Bukunya kayak seru-seru banget, tar aku cari deh', 1, 3),
('Terima kasih atas rekomendasinya! Aku akan mencoba membacanya.', 8, 3),
('Buku yang bagus untuk dibaca adalah "The Power of Now"', 144, 3),
('Saya merekomendasikan buku "Atomic Habits" untukmu!', 145, 3),
('Sudah membaca "Educated" dan sangat menginspirasi!', 146, 3),
('Bagiku, "The Alchemist" adalah buku yang luar biasa.', 147, 3),
('Buku yang sangat saya sukai adalah "To Kill a Mockingbird"', 148, 3),
('Terima kasih! "The Subtle Art of Not Giving a F*ck" patut dibaca!', 149, 3),
('Aku merekomendasikan buku "Norwegian Wood" karya Haruki Murakami.', 150, 3),
('Saya sangat menikmati "The Book Thief", kamu harus membacanya!', 151, 3),
('Buku yang patut kamu baca adalah "The Catcher in the Rye"', 152, 3),
('Saya merekomendasikan "1984" oleh George Orwell.', 153, 3),
('Buku bagus yang wajib dibaca adalah "The Great Gatsby"', 154, 3),
('Terima kasih atas rekomendasinya! Aku akan mencarinya.', 155, 3),
('Buku yang sangat saya suka adalah "Pride and Prejudice"', 156, 3),
('Saya merekomendasikan "Brave New World" oleh Aldous Huxley.', 157, 3);
