-- Membuat database "threadsdb"
CREATE DATABASE threadsdb;

-- Menggunakan database "threadsdb"
USE threadsdb;

-- Membuat tabel "users"
CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  email VARCHAR(100) UNIQUE NOT NULL, -- unique key
  username VARCHAR(50) UNIQUE NOT NULL, -- unique key
  fullname VARCHAR(100) NOT NULL,
  birthday DATE,
  profile_picture VARCHAR(200),
  bio TEXT
);

-- Membuat tabel "threads"
CREATE TABLE threads (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  threads_text TEXT NOT NULL,
  media VARCHAR(200),
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id) -- foreign key
);

-- Membuat tabel "threads_likes"
CREATE TABLE threads_likes (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  user_id INT NOT NULL,
  threads_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id), -- foreign key
  FOREIGN KEY (threads_id) REFERENCES threads(id) -- foreign key
);

-- Membuat tabel "re_threads"
CREATE TABLE re_threads (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  quote_text TEXT,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT NOT NULL,
  threads_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id), -- foreign key
  FOREIGN KEY (threads_id) REFERENCES threads(id) -- foreign key
);

-- Membuat tabel "threads_replies"
CREATE TABLE threads_replies (
  id INT PRIMARY KEY AUTO_INCREMENT, -- primary key
  reply_text TEXT NOT NULL,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT,
  threads_id INT,
  parent_reply_id INT,
  FOREIGN KEY (user_id) REFERENCES users(id), -- foreign key
  FOREIGN KEY (threads_id) REFERENCES threads(id), -- foreign key
  FOREIGN KEY (parent_reply_id) REFERENCES threads_replies(id) -- foreign key
);
