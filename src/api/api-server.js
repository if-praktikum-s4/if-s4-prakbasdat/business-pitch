const express = require('express');
const mysql = require('mysql');
const path = require('path');

const app = express();

const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '********',
  database: 'threadsdb',
});

db.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL database:', err);
    return;
  }
  console.log('Connected to MySQL database');
});

// API routes 
app.get('/api/users', (req, res) => {
  // Fetch users from the database
  db.query('SELECT * FROM users', (err, results) => {
    if (err) {
      console.error('Error fetching users:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ users: results });
  });
});

// Endpoint untuk mendapatkan user berdasarkan ID
app.get('/api/users/:id', (req, res) => {
  const { id } = req.params;
  const query = 'SELECT * FROM users WHERE id = ?';
  
  db.query(query, id, (err, result) => {
    if (err) {
      console.error('Error fetching user:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    
    if (result.length === 0) {
      res.status(404).json({ error: 'User not found' });
      return;
    }
    
    res.json({ user: result[0] });
  });
});


app.get('/api/threads', (req, res) => {
  db.query('SELECT * FROM threads', (err, results) => {
    if (err) {
      console.error('Error fetching threads:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ threads: results });
  });
});

app.get('/api/likes', (req, res) => {
  db.query('SELECT * FROM threads_likes', (err, results) => {
    if (err) {
      console.error('Error fetching threads likes:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ threadsLikes: results });
  });
});

app.get('/api/replies', (req, res) => {
  db.query('SELECT * FROM threads_replies', (err, results) => {
    if (err) {
      console.error('Error fetching threads replies:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ threadsReplies: results });
  });
});

app.get('/api/rethreads', (req, res) => {
  db.query('SELECT * FROM re_threads', (err, results) => {
    if (err) {
      console.error('Error fetching re-threads:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ reThreads: results });
  });
});

// Endpoint untuk operasi POST pada tabel 'users'
app.post('/api/users', (req, res) => {
  const { email, username, fullname, birthday, profile_picture, bio } = req.body;
  const query = 'INSERT INTO users (email, username, fullname, birthday, profile_picture, bio) VALUES (?, ?, ?, ?, ?, ?)';
  const values = [email, username, fullname, birthday, profile_picture, bio];
  
  db.query(query, values, (err, result) => {
    if (err) {
      console.error('Error creating user:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ message: 'User created successfully' });
  });
});

// Endpoint untuk operasi PUT pada tabel 'users' berdasarkan ID
app.put('/api/users/:id', (req, res) => {
  const { id } = req.params;
  const { email, username, fullname, birthday, profile_picture, bio } = req.body;
  const query = 'UPDATE users SET email = ?, username = ?, fullname = ?, birthday = ?, profile_picture = ?, bio = ? WHERE id = ?';
  const values = [email, username, fullname, birthday, profile_picture, bio, id];
  
  db.query(query, values, (err, result) => {
    if (err) {
      console.error('Error updating user:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ message: 'User updated successfully' });
  });
});

// Endpoint untuk operasi DELETE pada tabel 'users' berdasarkan ID
app.delete('/api/users/:id', (req, res) => {
  const { id } = req.params;
  const query = 'DELETE FROM users WHERE id = ?';
  
  db.query(query, id, (err, result) => {
    if (err) {
      console.error('Error deleting user:', err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json({ message: 'User deleted successfully' });
  });
});


// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

const port = 8080;
app.listen(port, () => {
  console.log(`Server api berjalan pada port ${port}`);
});
